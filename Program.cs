﻿using System;

namespace exercise_22
{
    class Program
    {
        public static void Main(string[] args)
        {
        
        //Create an Array
        var movies = new string[5] {"Finding Nemo", "The Lion King", "Bugs Life", "Frozen", "Beauty and The Beast"};
        
        //Sort the Array in Alphabetical Order
        Array.Sort(movies);
        Array.Reverse(movies);

        var output = string.Join(",", movies);

        var opinion = "The Lion King is the best movie out there from disney!";
        Console.WriteLine(opinion);

        //Print the Array to the screen
        Console.WriteLine(string.Join(",", movies));
        Console.WriteLine(output);

        }
    }
}
